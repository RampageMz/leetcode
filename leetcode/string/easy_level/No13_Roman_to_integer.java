package string.easy_level;

import java.util.HashMap;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/14 19:03
 * <p>
 * Content:    No13_Roman_to_integer:
 **/

public class No13_Roman_to_integer {

    /**
     * 罗马字符转化成数字
     * VII IV XVI XIIV XX XL(40) XXL(30) XV
     *
     * @param s 罗马字符串
     * @return 整数
     */
    public int romanToInt(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);

        int sum = 0;

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (i + 1 < s.length()) {
                int cur = map.get(ch);
                int next = map.get(s.charAt(i + 1));
                if (cur > next) {   // XV
                    sum += cur;
                } else {   //XXL VIII XXI
                    int j = i;
                    while (j + 1 < s.length() && s.charAt(j) == s.charAt(j + 1)) {
                        j++;
                    }
                    int continues = (j - i + 1) * map.get(s.charAt(j));
                    if (j + 1 >= s.length()) {
                        sum += continues;
                        return sum;
                    } else if (map.get(s.charAt(j)) < map.get(s.charAt(j + 1))) {
                        sum += map.get(s.charAt(j + 1)) - continues;
                        i = j + 1;
                    } else {
                        sum += continues;
                        i = j;
                    }
                }
            } else { // 当前最后一个数
                sum += map.get(ch);
            }
        }

        return sum;
    }

    public static void main(String[] args) {
        No13_Roman_to_integer solution = new No13_Roman_to_integer();
        System.out.println(solution.romanToInt("DCXXI"));
//        System.out.println(solution.romanToInt("IV"));
//        System.out.println(solution.romanToInt("IX"));
//        System.out.println(solution.romanToInt("LVIII"));
//        System.out.println(solution.romanToInt("MCMXCIV"));
    }
}
