package string.easy_level;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/20 15:12
 * <p>
 * Content:    No937_Reorder_Log_Files:
 **/

public class No937_Reorder_Log_Files {

    /**
     * 字母日志按字典序排序 数字日志放后面 相对顺序不变
     *
     * comparetor 1  s1>s2 需要交换
     *
     * @param logs
     * @return
     */
    public String[] reorderLogFiles(String[] logs) {

        Comparator<String> myComp = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                int s1si = s1.indexOf(' ');
                int s2si = s2.indexOf(' ');
                char s1fc = s1.charAt(s1si+1);
                char s2fc = s2.charAt(s2si+1);

                if (s1fc <= '9') {
                    if (s2fc <= '9') return 0;
                    else return 1;
                }
                if (s2fc <= '9') return -1;

                int preCompute = s1.substring(s1si+1).compareTo(s2.substring(s2si+1));
                if (preCompute == 0) return s1.substring(0,s1si).compareTo(s2.substring(0,s2si));
                return preCompute;
            }
        };

        Arrays.sort(logs, myComp);
        return logs;
    }

    public static void main(String[] args) {

    }
}
