package string.easy_level;

import java.util.HashMap;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/15 20:14
 * <p>
 * Content:    No383_Ransom_Note:
 **/

public class No383_Ransom_Note {

    /**
     * 从杂志中找到字母构成赎金信中的文字 杂志中的文字只能用一次
     *
     * @param ransomNote 赎金信
     * @param magazine   杂志
     * @return T/F
     */
    public static boolean canConstruct(String ransomNote, String magazine) {
        if (magazine == null || magazine.length() < ransomNote.length())
            return false;

        if (ransomNote.equals(magazine))
            return true;

        HashMap<Character, Integer> map = new HashMap<>();
        //int count = 0;
        for (int i = 0; i < magazine.length(); ++i) {
            if (map.containsKey(magazine.charAt(i)))
                map.put(magazine.charAt(i), map.get(magazine.charAt(i)) + 1);
            else
                map.put(magazine.charAt(i), 1);
        }

        for (int i = 0; i < ransomNote.length(); ++i) {
            char ch = ransomNote.charAt(i);
            if (map.containsKey(ch) && map.get(ch) > 0)
                map.put(ch, map.get(ch) - 1);
            else
                return false;
        }

        return true;
    }

    /**
     * Better
     * 将char映射到int数组中
     * @param ransomNote
     * @param magazine
     * @return
     */
    public boolean BetterCanConstruct(String ransomNote,String magazine){
        int[] array = new int[128];
        int mLen = magazine.length();
        for(int i=0;i<mLen;i++){
            array[magazine.charAt(i)]++;
        }
        int rLen = ransomNote.length();
        for(int i=0;i<rLen;i++){
            if(array[ransomNote.charAt(i)] > 0)
                array[ransomNote.charAt(i)]--;
            else
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(canConstruct("", ""));
    }
}
