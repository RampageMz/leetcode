package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/10 14:21
 * <p>
 * Content:    No557_Reverse_Words_in_a_String_III:
 **/

public class No557_Reverse_Words_in_a_String_III {

    /**
     * 翻转语句中的每个单词
     *
     * @param s 多个单词的字符串
     * @return 翻转后的字符串
     */
    public static String reverseWords(String s) {
        String[] strings = s.trim().split("\\s+");  // TODO: 2018/12/10 \\s+ 代表一个或者多个空格  s代表空格（tab 空格 换行都算） + 代表一个或多个

        StringBuffer stringBuffer = new StringBuffer();
        for (String str : strings) {
            stringBuffer.append(new StringBuffer(str).reverse().toString() + " ");
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);

        return stringBuffer.toString();
    }

    /**
     * Better 遍历
     * @param s
     * @return
     */
    public String BetterReverseWords(String s)
    {
        char[] ca = s.toCharArray();
        for (int i = 0; i < ca.length; i++) {
            if (ca[i] != ' ') {   // when i is a non-space
                int j = i;
                while (j + 1 < ca.length && ca[j + 1] != ' ') { j++; } // move j to the end of the word
                reverse(ca, i, j);
                i = j;
            }
        }
        return new String(ca);
    }

    private void reverse(char[] ca, int i, int j) {
        for (; i < j; i++, j--) {
            char tmp = ca[i];
            ca[i] = ca[j];
            ca[j] = tmp;
        }
    }

    public static void main(String[] args) {
        System.out.println(reverseWords("Let's take LeetCode contest"));
    }
}
