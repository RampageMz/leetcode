package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/10 14:13
 * <p>
 * Content:    No657_Robot_Return_to_Origin:
 **/

public class No657_Robot_Return_to_Origin {

    /**
     * xy平面 机器人上下左右 从0出发  是否可以回到起点
     *
     * @param moves 方向字符串
     * @return T/F
     */
    public boolean judgeCircle(String moves) {
        if (moves == null || moves.length() <= 1)
            return false;

        int x_sum = 0, y_sum = 0;

        for (int i = 0; i < moves.length(); i++) {
            char move = moves.charAt(i);
            switch (move) {
                case 'U':
                    y_sum++;
                    break;
                case 'D':
                    y_sum--;
                    break;
                case 'L':
                    x_sum--;
                    break;
                case 'R':
                    x_sum++;
                    break;
            }
        }

        if (x_sum == 0 && y_sum == 0)
            return true;
        else
            return false;
    }

    public static void main(String[] args) {

    }
}
