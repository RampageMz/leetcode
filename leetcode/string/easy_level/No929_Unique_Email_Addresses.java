package string.easy_level;

import java.util.HashSet;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/08 16:00
 * <p>
 * Content:    No929_Unique_Email_Addresses:
 **/

public class No929_Unique_Email_Addresses {

    /**
     * 邮箱地址识别： 识别出多少个不同的域名
     * local中 .可以忽略 +后面一直到@的部分忽略
     *
     * @param emails
     * @return
     */
    public int numUniqueEmails(String[] emails) {
        HashSet set = new HashSet();

        for (String s : emails) {
            String[] strs = s.split("@");
            String[] localSplit = strs[0].split("\\+");
            set.add(localSplit[0].replaceAll(".", "") + "@" + strs[1]);
        }

        return set.size();
    }

    public static void main(String[] args) {

    }
}
