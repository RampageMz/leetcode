package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/20 14:29
 * <p>
 * Content:    No917_Reverse_Only_Letters:
 **/

public class No917_Reverse_Only_Letters {

    /**
     * 翻转字符串，如果不是字母 留在原地
     *
     * @param S 原字符串
     * @return 新字符串
     */
    public String reverseOnlyLetters(String S) {
        char[] arr = S.toCharArray();
        int len = arr.length;
        int left = 0, right = len - 1;

        while (left < right) {
            if (!isLetter(arr[left]))
                left++;
            else if (!isLetter(arr[right]))
                right--;
            else {
                char tmp = arr[left];
                arr[left] = arr[right];
                arr[right] = tmp;
                left++;
                right--;
            }
        }

        return new String(arr);
    }

    public boolean isLetter(char ch) {
        return (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z') ? true : false;
    }

    public static void main(String[] args) {
        No917_Reverse_Only_Letters solution = new No917_Reverse_Only_Letters();
        System.out.println(solution.reverseOnlyLetters("a-bC-dEf-ghIj"));
        System.out.println(solution.reverseOnlyLetters("Test1ng-Leet=code-Q!"));
        System.out.println(solution.reverseOnlyLetters("ab-cd"));
    }
}
