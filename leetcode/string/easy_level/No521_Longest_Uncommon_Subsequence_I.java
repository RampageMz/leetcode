package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/10 21:33
 * <p>
 * Content:    No521_Longest_Uncommon_Subsequence_I:
 **/

public class No521_Longest_Uncommon_Subsequence_I {

    /**
     * 找到独有的最长子串
     *
     * @param a 字符串
     * @param b 字符串
     * @return 长度
     */
    public int findLUSlength(String a, String b) {
        return a.equals(b) ? -1 : Math.max(a.length(), b.length());
    }

    public static void main(String[] args) {

    }
}
