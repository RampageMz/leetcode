package string.easy_level;

import java.util.HashMap;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/20 15:38
 * <p>
 * Content:    No387_First_Unique_Character_in_a_String:
 **/

public class No387_First_Unique_Character_in_a_String {

    /**
     * 字符串中第一个不重复的字符
     *
     * @param s 字符串
     * @return 索引下标 0 开始
     */
    public static int firstUniqChar(String s) {
        HashMap<Character, int[]> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (!map.containsKey(ch)) {
                map.put(ch, new int[]{1, i});
            } else {
                int[] val = map.get(ch);
                map.put(ch, new int[]{val[0] + 1, val[1]});
            }
        }

        int index = Integer.MAX_VALUE;
        boolean found = false;
        for (int[] val : map.values()) {
            if (val[0] == 1) {
                found = true;
                index = Math.min(index, val[1]);
            }
        }

        if (found)
            return index;

        return -1;
    }

    /**
     * Better
     * <p>
     * 映射到字母索引的长度为26的数组中
     *
     * @param s
     * @return
     */
    public int better(String s) {
        int freq[] = new int[26];
        for (int i = 0; i < s.length(); i++)
            freq[s.charAt(i) - 'a']++;
        for (int i = 0; i < s.length(); i++)
            if (freq[s.charAt(i) - 'a'] == 1)
                return i;
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(firstUniqChar("leetcode"));
        System.out.println(firstUniqChar("loveleetcode"));
    }
}
