package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/11 16:21
 * <p>
 * Content:    No788_Rotated_Digits:
 **/

public class No788_Rotated_Digits {

    /**
     * 180度旋转数字后不同的数
     *
     * @param N 从1到N的整数
     * @return 个数
     */
    public int rotatedDigits(int N) {
        int[] flag = new int[N + 1];
        int count = 0;

        // flag=0 无效 flag=1 有效但是翻转后数字相同 flag=2 有效且不同
        for (int i = 0; i <= N; i++) {
            if (i < 10) {
                if (i == 0 || i == 1 || i == 8)
                    flag[i] = 1;
                else if (i == 2 || i == 5 || i == 6 || i == 9) {
                    flag[i] = 2;
                    count++;
                }
            } else {
                int first = flag[i / 10], end = flag[i % 10];   // TODO: 2018/12/11 很牛逼的感觉
                if (first == 1 && end == 1)
                    flag[i] = 1;
                else if (first >=1 && end >= 1) {
                    flag[i] = 2;
                    count++;
                }
            }
        }

        return count;
    }

    /**
     * Better
     * @param N
     * @return
     */
    public int BetterRotatedDigits(int N) {
        // Count how many n in [1, N] are good.
        int ans = 0;
        for (int n = 1; n <= N; ++n)
            if (good(n, false)) ans++;
        return ans;
    }

    // Return true if n is good.
    // The flag is true iff we have an occurrence of 2, 5, 6, 9.
    public boolean good(int n, boolean flag) {
        if (n == 0) return flag;

        int d = n % 10;
        if (d == 3 || d == 4 || d == 7) return false;
        if (d == 0 || d == 1 || d == 8) return good(n / 10, flag);
        return good(n / 10, true);
    }

    public static void main(String[] args) {

    }
}
