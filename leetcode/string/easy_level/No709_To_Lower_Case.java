package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/08 16:24
 * <p>
 * Content:    No709_To_Lower_Case:
 **/

public class No709_To_Lower_Case {

    /**
     * 字符串 全转化成小写
     * <p>
     * 0:48 A:65 a:97
     *
     * @param str 字符串
     * @return
     */
    public String toLowerCase(String str) {
        char[] s = str.toCharArray();
        for (int i = 0; i < s.length; i++) {
            if (s[i] <= 'Z' && s[i] >= 'A') {
                s[i] = (char) (s[i] + 'a' - 'A');
            }
        }

        return new String(s);
    }

    public static void main(String[] args) {

    }
}
