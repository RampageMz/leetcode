package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/14 17:44
 * <p>
 * Content:    No696_Count_Binary_Substrings:
 **/

public class No696_Count_Binary_Substrings {

    /**
     * 计算具有相同数量0和1的非空(连续)子字符串的数量，并且这些子字符串中的所有0和所有1都是组合在一起的。
     * 重复出现的子串要计算它们出现的次数
     *
     * @param s 输入的字符串
     * @return 次数
     */
    public int countBinarySubstrings(String s) {
        int prevRunLength = 0, curRunLength = 1, res = 0;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) curRunLength++;
            else {
                prevRunLength = curRunLength;
                curRunLength = 1;
            }
            if (prevRunLength >= curRunLength) res++;
        }
        return res;
    }

    public static void main(String[] args) {
        No696_Count_Binary_Substrings solution = new No696_Count_Binary_Substrings();
        System.out.println(solution.countBinarySubstrings("00110011"));
        System.out.println(solution.countBinarySubstrings("10101"));
    }
}
