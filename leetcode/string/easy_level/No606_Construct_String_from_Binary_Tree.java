package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/14 18:04
 * <p>
 * Content:    No606_Construct_String_from_Binary_Tree:
 **/

public class No606_Construct_String_from_Binary_Tree {

    public String tree2str(TreeNode t) {
        if (t == null) return "";

        String result = t.val + "";

        String left = tree2str(t.left);
        String right = tree2str(t.right);

        if (left.equals("") && right.equals(""))
            return result;
        if (left.equals(""))
            return result + "()" + "(" + right + ")";
        if (right.equals(""))
            return result + "(" + left + ")";

        return result + "(" + left + ")" + "(" + right + ")";
    }

    public static void main(String[] args) {
        No606_Construct_String_from_Binary_Tree solution = new No606_Construct_String_from_Binary_Tree();
        TreeNode t1 = new TreeNode(1);
        t1.left = new TreeNode(2);
        t1.right = new TreeNode(3);
        t1.left.left = new TreeNode(4);
        System.out.println(solution.tree2str(t1));
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
