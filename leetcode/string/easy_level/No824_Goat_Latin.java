package string.easy_level;

import java.util.HashMap;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/10 21:41
 * <p>
 * Content:    No824_Goat_Latin:
 **/

public class No824_Goat_Latin {

    /**
     * 如果单词以元音开头（a, e, i, o, u），在单词后添加"ma"。
     * 例如，单词"apple"变为"applema"。
     * <p>
     * 如果单词以辅音字母开头（即非元音字母），移除第一个字符并将它放到末尾，之后再添加"ma"。
     * 例如，单词"goat"变为"oatgma"。
     * <p>
     * 根据单词在句子中的索引，在单词最后添加与索引相同数量的字母'a'，索引从1开始。
     * 例如，在第一个单词后添加"a"，在第二个单词后添加"aa"，以此类推。
     *
     * @param S 源字符串
     * @return 新串
     */
    public static String toGoatLatin(String S) {
        HashMap<Character, Boolean> map = new HashMap<>();
        map.put('a', true);
        map.put('e', true);
        map.put('i', true);
        map.put('o', true);
        map.put('u', true);
        map.put('A', true);
        map.put('E', true);
        map.put('I', true);
        map.put('O', true);
        map.put('U', true);

        String[] strings = S.trim().split("\\s+");
        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < strings.length; i++) {
            char first = strings[i].charAt(0);
            String newString = "";
            if (map.containsKey(first)) {
                //a e i o u开头
                newString = strings[i] + "ma";
            } else {
                //其他开头 首字母移到后面
                newString = firstToEnd(strings[i]) + "ma";
            }

            for (int space = 0; space <= i; space++)
                newString += "a";

            if (i != strings.length - 1)
                newString += " ";

            stringBuffer.append(newString);
        }

        return stringBuffer.toString();
    }

    /**
     * 将首字母移到末尾
     *
     * @param s
     */
    private static String firstToEnd(String s) {
        char[] chars = s.toCharArray();
        char first = chars[0];
        for (int i = 0; i < chars.length - 1; i++) {
            chars[i] = chars[i + 1];
        }

        chars[chars.length - 1] = first;
        return new String(chars);
    }

    private void reverse(char[] s) {
        int i = 0, j = s.length - 1;
        while (i < j) {
            char ch = s[i];
            s[i] = s[j];
            s[j] = ch;
        }
    }

    public static void main(String[] args) {
        System.out.println(toGoatLatin("I speak Goat Latin"));
        System.out.println(toGoatLatin("The quick brown fox jumped over the lazy dog"));
    }
}
