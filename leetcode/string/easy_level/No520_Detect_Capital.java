package string.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/11 10:59
 * <p>
 * Content:    No520_Detect_Capital:
 **/

public class No520_Detect_Capital {

    /**
     * 所有字母都是大写
     * 所有字母都是小写
     * 只有首字母大写
     *
     * @param word 字符串
     * @return T/F
     */
    public boolean detectCapitalUse(String word) {
        int count = 0;
        for (char ch : word.toCharArray()) {
            if ('Z' - ch >= 0)  // 大写
                count++;
        }

        // 没有大写 大写数量等于字符串长度 大写数量为1 且在第一个
        return (count == 0 || count == word.length() || (count == 1) && ('Z' - word.charAt(0)) >= 0);
    }

    public boolean isUpperCase(char ch) {
        return ch >= 'A' && ch <= 'Z';
    }

    public static void main(String[] args) {
        System.out.println(new No520_Detect_Capital().detectCapitalUse("USA"));
        System.out.println(new No520_Detect_Capital().detectCapitalUse("FlaG"));
        System.out.println(new No520_Detect_Capital().detectCapitalUse("leetcode"));
    }
}
