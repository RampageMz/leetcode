package string.easy_level;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/10 13:39
 * <p>
 * Content:    No804_Unique_Morse_Code_Words:
 **/

public class No804_Unique_Morse_Code_Words {

    /**
     * 摩斯电码
     *
     * @param words 字符串数组
     * @return 不同电码的个数
     */
    public static int uniqueMorseRepresentations(String[] words) {
        String[] morseArray = new String[]{
                ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."
        };
        HashMap<Character, String> morseMap = new HashMap<>();
        HashSet set = new HashSet();
        int index = 0;
        char ch = 'a';
        while (ch <= 'z') {
            morseMap.put(ch, morseArray[index++]);
            ch = (char) (ch + 1);
        }

        for (String s : words) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                sb.append(morseMap.get(c));
            }
            set.add(new String(sb));
        }

        return set.size();
    }

    public static void main(String[] args) {
        System.out.println(uniqueMorseRepresentations(new String[]{
                "gin", "zen", "gig", "msg"
        }));
    }
}
