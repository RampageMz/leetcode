package array.util;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/27 13:55
 * <p>
 * Content:    ToolFunction:
 **/

public class ToolFunction {

    /**
     * 交换整型数组的两个数
     *
     * @param a 数组
     * @param i 第一个数下标
     * @param j 第二个数下标
     */
    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    /**
     * 交换矩阵的两个点值
     * @param a 矩阵
     * @param x1 第一个点的x
     * @param y1 第一个点的y
     * @param x2 第二个点的x
     * @param y2 第二个点的y
     */
    public static void swapMatrix(int[][] a, int x1, int y1, int x2, int y2) {
        int tmp = a[x1][y1];
        a[x1][y1] = a[x2][y2];
        a[x2][y2] = tmp;
    }

    /**
     * 输出一位数组信息
     *
     * @param num 一维数组
     */
    public static void printArray(int[] num, boolean showDetail) {
        if (num == null || num.length == 0) {
            System.out.println("数组为空!");
            return;
        }

        System.out.println("数组： size:" + num.length);

        if (showDetail) {
            for (int i = 0; i < num.length; i++) {
                System.out.println("[" + i + "]: " + num[i]);
            }
        }
        System.out.println(Arrays.toString(num));
        System.out.println("--- --- --- --- ---");
    }

    public static void printDoubelArray(int[][] num) {
        if (num == null || num.length == 0) {
            System.out.println("数组为空!");
            return;
        }

        System.out.println("数组： line:" + num.length + " row:" + num[0].length);
        for (int i = 0; i < num.length; i++) {
            int[] a = num[i];
            System.out.print("[" + i + "]: ");
            System.out.println(Arrays.toString(a));
        }

        System.out.println("--- --- --- --- ---");
    }

    /**
     * 输出索引头
     *
     * @param a 数组
     */
    public static void printIndex(int[] a) {
        System.out.print("[");
        int length = a.length;
        while (length-- > 0) {
            System.out.print(a.length - length - 1);
            if (length > 0) {
                System.out.print(", ");
            }
        }

        System.out.println("]");
    }

}
