package array.easy_level;

import java.util.HashSet;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/30 15:40
 * <p>
 * Content:    No217_Contains_Duplicate:
 **/

public class No217_Contains_Duplicate {

    /**
     * 数组里是否有重复元素
     *
     * @param nums
     * @return
     */
    public boolean containsDuplicate(int[] nums) {
        if (nums == null || nums.length == 0)
            return false;

        HashSet<Integer> set = new HashSet<>();

        // 或者一边 add 一边判断contains
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }

        if (set.size() == nums.length)
            return false;
        else
            return true;
    }

    public static void main(String[] args) {

    }
}
