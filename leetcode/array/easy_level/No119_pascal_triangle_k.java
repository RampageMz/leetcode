package array.easy_level;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/23 15/20
 * <p>
 * Content:    No119_pascal_triangle_k:
 **/

public class No119_pascal_triangle_k {

    /**
     * Pascal 三角形，输出第k行的数组，k从0开始
     * @param rowIndex k
     * @return
     */
    public static List<Integer> getRow(int rowIndex) {
        List<Integer> list=new ArrayList<>();
        if (rowIndex < 0)
            return list;

        for (int i = 0; i < rowIndex + 1; i++) {
            list.add(0, 1);     // TODO: 2018/11/23 list.add(index,value)当index相同时会强心插入，依次后移
            for (int j = 1; j < list.size() - 1; j++) {
                list.set(j, list.get(j) + list.get(j + 1));
            }
        }
        return list;
    }

    public static void main(String[] args) {
        System.out.println(getRow(7));

        List<Integer> list=new ArrayList<>();
        list.add(0,1);
        list.add(0,2);
        list.add(0,3);
        System.out.println(list);   // 3,2,1
    }
}
