package array.easy_level;

import array.util.ToolFunction;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/28 11:49
 * <p>
 * Content:    No922_Sort_Array_By_Parity_II:
 **/

public class No922_Sort_Array_By_Parity_II {

    /**
     * 数组中奇偶个数相同，奇数放在奇数下标上
     * 奇偶两个指针
     *
     * @param A 无序数组
     * @return
     */
    public static int[] sortArrayByParityII(int[] A) {
        int even_index = 0;     // 偶数下标
        int odd_index = 1;      // 奇数下标

        // 奇偶个数相等 如果偶数有一个错位，奇数必有
        while (even_index <= A.length - 2 && odd_index <= A.length - 1) {
            while (even_index<=A.length-2 && A[even_index] % 2 == 0)  // 找到偶数位第一个不符合条件的
                even_index += 2;

            if (even_index == A.length)     // =时 已经遍历结束
                break;

            while (A[odd_index] % 2 == 1)
                odd_index += 2;

            ToolFunction.swap(A, even_index, odd_index);
            even_index+=2;
            odd_index+=2;
        }

        return A;
    }

    public static void main(String[] args) {
        int[] a = new int[]{4, 2, 5, 7,7,6,4,3};
        ToolFunction.printIndex(a);
        ToolFunction.printArray(a,false);
        System.out.println(Arrays.toString(sortArrayByParityII(a)));
    }
}
