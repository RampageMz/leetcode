package array.easy_level;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/30 14:49
 * <p>
 * Content:    No169_Majority_Element:
 **/

public class No169_Majority_Element {

    /**
     * 找到数组里出现次数超过n/2次数的数
     *
     * @param nums 数组
     * @return 出现次数多的数
     */
    public static int majorityElement(int[] nums) {
        int major = nums[0], count = 1;

        // 相当于从头开始 遍历 每一次都判断下 major>0当前major是否为最好的 major=0 需要替换新的数
        // 2 2 1 major=2 count=1
        // 2 2 1 1 major=2 count=0  当前没有最好的数，但是major仍然保留之前的数
        // 2 2 1 1 1 major=1 count=1 count相当于是当前出现最多的数与其他数出现次数的差 差=0 说明 2 2 1 3 这种
        // 差>0 说明有的书已经出现次数超过一半
        for (int i = 1; i < nums.length; i++) {
            if (count == 0) {
                major = nums[i];
                count++;
            } else if (nums[i] == major)
                count++;
            else
                count--;
        }

        return major;
    }

    // 先排序 再取中间
    public static int sort(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

    // hashMap
    public static int hashMap(int[] nums){
        Map<Integer, Integer> myMap = new HashMap<Integer, Integer>();
        int ret=0;
        for (int num: nums) {
            if (!myMap.containsKey(num))    // 没出现过的放入map
                myMap.put(num, 1);
            else
                myMap.put(num, myMap.get(num)+1);   // 出现过的次数+1
            if (myMap.get(num)>nums.length/2) {
                ret = num;
                break;
            }
        }
        return ret;
    }

    public static void main(String[] args) {
        System.out.println(majorityElement(new int[]{2, 3, 2, 1, 2, 1, 2, 2}));
    }
}
