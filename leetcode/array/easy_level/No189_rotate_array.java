package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/24 15/11
 * <p>
 * Content:    No189_rotate_array:
 **/

public class No189_rotate_array {

    /**
     * 将数组后面的部分挪到前面
     * beat 16%
     *
     * @param nums 数组
     * @param k    k次
     */
    public static void rotate(int[] nums, int k) {
        if (nums == null || nums.length == 0)
            return;

        k = k % nums.length;    // 只要求余一次就可以

        while (k-- > 0) {
            moveLastToHead(nums);
        }
    }

    public static void moveLastToHead(int[] nums) {
        int temp = nums[nums.length - 1];
        for (int i = nums.length - 2; i >= 0; i--) {
            nums[i + 1] = nums[i];
        }
        nums[0] = temp;
    }


    /**
     * Better 解法
     *
     * @param nums 数组
     * @param k    k次
     */
    public void BetterRotate(int[] nums, int k) {   // 1 2 3 4 5 6 7
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);  // 倒序整个数组 7 6 5 4 3 2 1
        reverse(nums, 0, k - 1);    // 再倒序前面k个，使其正过来 5 6 7 4 3 2 1
        reverse(nums, k, nums.length - 1); // 再倒序剩下的所有  5 6 7 1 2 3 4
    }

    // 头尾指针，倒序整个数组
    private void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1, 2, 3, 4, 5, 6, 7};
        rotate(nums, 3);
        System.out.println(Arrays.toString(nums));
    }
}
