package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/02 16:47
 * <p>
 * Content:    No674_Longest_Continuous_Increasing_Subsequence:
 **/

public class No674_Longest_Continuous_Increasing_Subsequence {

    /**
     * 找到数组递增的最大长度
     *
     * @param nums 数组
     * @return 长度
     */
    public static int findLengthOfLCIS(int[] nums) {
        if (nums == null || nums.length == 0)
            return 0;

        int max = 1, maxHere = 1;
        for (int i = 1; i < nums.length; ++i) {
            if (nums[i] > nums[i - 1])
                maxHere += 1;
            else
                maxHere = 1;

            max = Math.max(max, maxHere);
        }

        return max;
    }

    public static void main(String[] args) {
        // System.out.println(findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}));
        // System.out.println(findLengthOfLCIS(new int[]{2, 2, 2, 2, 2}));
        System.out.println(findLengthOfLCIS(new int[]{1, 3, 5, 4, 2, 3, 4, 5}));
    }
}
