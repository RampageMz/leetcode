package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/22 20/47
 * <p>
 * Content:    No11_container_with_most_water: https://leetcode.com/problems/container-with-most-water/
 **/

public class No11_container_with_most_water {

    /**
     * 头尾指针向中间遍历，记录当前包裹的面试，两者中短的那个指针向中间靠拢
     *
     * @param height:高度数组
     * @return area
     */
    public static int maxArea(int[] height) {
        int area, max = 0;
        int i = 0;
        int j = height.length - 1;

        while (i < j) {
            int height_i = height[i];
            int height_j = height[j];

            if (height_i < height_j) {
                area = (j - i) * height_i;
                System.out.println(String.format("[%d]%d-[%d]%d area:%d", i, height_i, j, height_j, area));
                while (height[++i] < height_i) ;    // 短的那个指针向中间靠拢，找比自己大的第一个数
            } else {
                area = (j - i) * height_j;
                System.out.println(String.format("[%d]%d-[%d]%d area:%d", i, height_i, j, height_j, area));
                while (height[--j] < height_j) ;
            }
            max = Math.max(area, max);
            System.out.println("max:" + max + " " + i + "-" + j);
        }

        return max;
    }

    public static void main(String[] args) {
        System.out.println(maxArea(new int[]{
                1, 8, 6, 2, 5, 4, 8, 3, 7}));
    }
}
