package array.easy_level;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/04 16:00
 * <p>
 * Content:    No643_Maximum_Average_Subarray_I:
 **/

public class No643_Maximum_Average_Subarray_I {

    /**
     * 连续子数组平均值最大  指定连续数组的长度
     *
     * @param nums 数组
     * @param k    子数组长度
     * @return 平均值
     */
    public double findMaxAverage(int[] nums, int k) {
        if (nums == null || nums.length == 0)
            return 0;

        if (nums.length < k)
            return 0;

        int sum = 0, max = 0;
        //前k个
        for (int i = 0; i < k; ++i)
            sum += nums[i];
        max = sum;

        for (int i = k; i < nums.length; ++i) {
            sum += nums[i] - nums[i - k];   // 移动窗口一格 相当于减去第一个元素i-k 加上后一个元素i
            max = Math.max(max, sum);
        }

        return (max * 1.0) / k;
    }

    public static void main(String[] args) {

    }
}
