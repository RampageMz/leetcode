package array.easy_level;

import array.util.ToolFunction;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/27 11:19
 * <p>
 * Content:    No832_Flipping_an_Image:
 **/

public class No832_Flipping_an_Image {

    /**
     * 将矩阵每行先翻转，再讲0 1替换
     *
     * @param A 二维矩阵
     * @return
     */
    public static int[][] flipAndInvertImage(int[][] A) {
        if (A == null || A.length == 0)
            return A;

        int row = A[0].length;
        int line = A.length;

        for (int[] a : A) {
            int left = 0;
            int right = row - 1;
            while (left <= right) {
                if (left==right){
                    a[left] = (a[left] + 1) % 2;
                    break;
                }
                ToolFunction.swap(a, left, right);
                a[left] = (a[left] + 1) % 2;    // 0变成1 1变成0
                a[right] = (a[right] + 1) % 2;
                left++;
                right--;
            }
        }

        return A;
    }

    /**
     * Better
     *
     *
     * @param A
     * @return
     */
    public static int[][] BetterFlipAndInvertImage(int[][] A) {
        int n = A[0].length;

        for(int[] row: A) {
            for(int i=0;i+i<n;i++) {
                if(row[i]==row[n-1-i])      // TODO: 2018/11/27 &=两个均为true才为true |=两个有一个为true为true ^=两个相同为真
                    row[i]=row[n-1-i]^=1;   // 取巧的做法，如果头尾两个不同0 1或者1 0，先倒序，再0/1交换，其实和本身一样，不用做操作
                                            // 只要判断头尾相同的两个， 同时变0 或 1
                                            // a^=1 等于 a=a^1 二进制异或操作，对应二进制码，相同为0，不同为1
            }
        }

        return A;
    }

    public static void main(String[] args) {
        int[][] a = new int[][]{
                {1,1,0,0},{1,0,0,1},{0,1,1,1},{1,0,1,0}
        };
        ToolFunction.printDoubelArray(a);
        BetterFlipAndInvertImage(a);
        ToolFunction.printDoubelArray(a);

        int s=1;
        int ss=s^1;
        System.out.println(ss);
    }
}
