package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/05 20:54
 * <p>
 * Content:    No88_Merge_Sorted_Array:
 **/

public class No88_Merge_Sorted_Array {

    /**
     * 将两个有序数组归并到num1中
     * <p>
     * 倒序将俩数组扫描 值直接赋值进去
     *
     * @param nums1 第一个数组
     * @param m     第一个数组长度
     * @param nums2 第二个数组
     * @param n     第二个数组长度
     */
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int n1_index = m - 1;
        int n2_index = n - 1;
        int new_index = m + n - 1;  // 初始为新数组的末尾指针

        // 倒序扫描
        while (n1_index >= 0 && n2_index >= 0) {
            // 谁大 谁放到尾巴上  并且自身指针前移
            if (nums1[n1_index] > nums2[n2_index])
                nums1[new_index--] = nums1[n1_index--];
            else
                nums1[new_index--] = nums2[n1_index--];
        }

        //处理尾巴
        while (n2_index >= 0)
            nums1[new_index--] = nums2[n2_index--];
    }

    public static void main(String[] args) {

    }
}
