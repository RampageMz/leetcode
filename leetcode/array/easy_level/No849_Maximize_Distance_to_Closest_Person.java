package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/01 17:07
 * <p>
 * Content:    No849_Maximize_Distance_to_Closest_Person:
 **/

public class No849_Maximize_Distance_to_Closest_Person {

    /**
     * 1代表有人 0是空座位 找到一个座位，使得离自己最近人的距离，这个值最大
     *
     * @param seats 座位数组
     * @return 座位下标
     */
    public int maxDistToClosest(int[] seats) {
        int i, j, res = 0, n = seats.length;
        for (i = j = 0; j < n; ++j)     // ++j 等同于j++ 但效率更高
            if (seats[j] == 1) {
                if (i == 0)
                    res = Math.max(res, j - i);
                else
                    res = Math.max(res, (j - i + 1) / 2);

                i = j + 1;
            }
        res = Math.max(res, n - i);
        return res;
    }

    public int Better(int[] seats){
        int left = -1, maxDis = 0;
        int len = seats.length;

        for (int i = 0; i < len; i++) {
            if (seats[i] == 0) continue;

            if (left == -1) {
                maxDis = Math.max(maxDis, i);
            } else {
                maxDis = Math.max(maxDis, (i - left) / 2);
            }
            left = i;
        }

        if (seats[len - 1] == 0) {
            maxDis = Math.max(maxDis, len - 1 - left);
        }

        return maxDis;
    }

    public static void main(String[] args) {

    }
}
