package array.easy_level;

import java.util.stream.IntStream;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/01 16:33
 * <p>
 * Content:    No724_Find_Pivot_Index:
 **/

public class No724_Find_Pivot_Index {

    /**
     * 找到一个数，左边的数之和等于右边数之和
     *
     * @param nums 数组
     * @return 该数的下标
     */
    public static int pivotIndex(int[] nums) {
        if (nums == null || nums.length <= 2)
            return -1;

        int left_sum = 0;
        int sum = IntStream.of(nums).sum();

        // TODO: 2018/12/1 或者从左边开始累加 判断累加值*2 == sum-nums[i]
        for (int i = 0; i < nums.length; i++) {
            if (i != 0)
                left_sum += nums[i - 1];
            if (sum - left_sum - nums[i] == left_sum)
                return i;
        }

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(pivotIndex(new int[]{1, 7, 3, 6, 5, 6}));
        System.out.println(pivotIndex(new int[]{1, 2, 3}));
        System.out.println(pivotIndex(new int[]{-1, -1, -1, 0, 1, 1}));
    }
}
