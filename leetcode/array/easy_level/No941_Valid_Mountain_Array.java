package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/05 20:32
 * <p>
 * Content:    No941_Valid_Mountain_Array:
 **/

public class No941_Valid_Mountain_Array {

    /**
     * 像山一样的数组，山顶不能是第一个或者最后一个，递增或者递减时不能是相等
     *
     * @param A 数组
     * @return true/false
     */
    public static boolean validMountainArray(int[] A) {
        if (A == null || A.length < 3)
            return false;

        int left = 0, right = A.length - 1;

        while (left < A.length - 1 && A[left] < A[left + 1])
            left++;

        while (right > 0 && A[right - 1] > A[right])
            right--;

        if (left == A.length - 1 || right == 0)     // 判断单调增或者单调减
            return false;

        if (left == right)
            return true;

        return false;
    }

    public static void main(String[] args) {
        System.out.println(validMountainArray(new int[]{3, 5, 5, 4}));
        System.out.println(validMountainArray(new int[]{3, 5, 5}));
        System.out.println(validMountainArray(new int[]{2, 1}));
        System.out.println(validMountainArray(new int[]{0, 3, 2, 1}));
        System.out.println(validMountainArray(new int[]{1, 2, 3, 4, 5}));
    }
}
