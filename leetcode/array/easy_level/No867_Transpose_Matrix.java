package array.easy_level;

import array.util.ToolFunction;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/28 14:01
 * <p>
 * Content:    No867_Transpose_Matrix:
 **/

public class No867_Transpose_Matrix {

    /**
     * 矩阵转置 沿主对角线翻转
     *
     * @param A 矩阵
     * @return
     */
    public static int[][] transpose(int[][] A) {
        int row = A[0].length;
        int line = A.length;

        int[][] B = new int[row][line];

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < row; j++) {
                B[j][i] = A[i][j];
            }
        }

        return B;
    }

    public static void main(String[] args) {
        int[][] a = new int[][]{
                {1, 2, 3}, {4, 5, 6}, {7, 8, 9}
        };
        ToolFunction.printDoubelArray(transpose(a));

        int[][] b = new int[][]{
                {1, 2, 3}, {4, 5, 6}
        };
        ToolFunction.printDoubelArray(transpose(b));
    }
}
