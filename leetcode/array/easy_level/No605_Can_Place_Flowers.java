package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/06 20:04
 * <p>
 * Content:    No605_Can_Place_Flowers:
 **/

public class No605_Can_Place_Flowers {

    /**
     * 一个数组，0-没种花 1-种了花 但是两朵花不能相邻
     *
     * @param flowerbed 数组
     * @param n         要种几朵花
     * @return 能否种
     */
    public static boolean canPlaceFlowers(int[] flowerbed, int n) {
        int count = 0;
        for(int i = 0; i < flowerbed.length && count < n; i++) {
            if(flowerbed[i] == 0) {
                //get next and prev flower bed slot values. If i lies at the ends the next and prev are considered as 0.
                int next = (i == flowerbed.length - 1) ? 0 : flowerbed[i + 1];
                int prev = (i == 0) ? 0 : flowerbed[i - 1];
                if(next == 0 && prev == 0) {    // 前后都是0
                    flowerbed[i] = 1;
                    count++;
                }
            }
        }

        return count == n;

    }

    public static void main(String[] args) {
        //System.out.println(canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 1));
        //System.out.println(canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 2));
        System.out.println(canPlaceFlowers(new int[]{1, 0, 0, 0, 0, 1}, 2));
    }
}
