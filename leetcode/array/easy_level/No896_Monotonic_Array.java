package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/29 14:06
 * <p>
 * Content:    No896_Monotonic_Array:
 **/

public class No896_Monotonic_Array {

    /**
     * 单调数组 ， 单调增或者单调减都行
     *
     * @param A 数组
     * @return
     */
    public boolean isMonotonic(int[] A) {
        boolean inc = true, dec = true;
        for (int i = 1; i < A.length; i++) {
            if (A[i - 1] < A[i]) {
                dec = false;
            } else if (A[i - 1] > A[i]) {
                inc = false;
            }
//            inc &= (A[i - 1] <= A[i]);
//            dec &= (A[i - 1] >= A[i]);
        }
        return inc || dec;
    }

    public static void main(String[] args) {

    }
}
