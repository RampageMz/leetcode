package array.easy_level;

import java.util.HashSet;
import java.util.stream.IntStream;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/29 14:40
 * <p>
 * Content:    No888_Fair_Candy_Swap:
 **/

public class No888_Fair_Candy_Swap {

    /**
     * A[i] 是爱丽丝拥有的第 i 块糖的大小，B[j] 是鲍勃拥有的第 j 块糖的大小。
     * 因为他们是朋友，所以他们想交换一个糖果棒，这样交换后，他们都有相同的糖果总量
     * <p>
     * 交换的两个数 相差 应该为 |sumA-sumB|/2
     * sumA>sumB 则 A交换的数要比B交换的大
     *
     * @param A alice的糖果数组
     * @param B Bob的糖果数组
     * @return 0:alice要交换的糖果大小 1:bob要交换的糖果大小
     */
    public int[] fairCandySwap(int[] A, int[] B) {
        int sum_A = IntStream.of(A).sum();  // TODO: 2018/11/29 int数组求和 IntStream.Of(A).sum()
        int sum_B = IntStream.of(B).sum();
        int dif = (sum_A - sum_B) / 2;

        HashSet<Integer> set = new HashSet<>();

        for (int a : A) set.add(a);
        for (int b : B)
            if (set.contains(b + dif))
                return new int[]{b + dif, b};

        return new int[2];
    }

    public int[] BetterFairCandySwap(int[] A, int[] B) {
        boolean[] bSet = new boolean[100001];   // 100001对应的是B数组中每个数的最大值
        int aTotal = IntStream.of(A).sum();
        int bTotal = IntStream.of(B).sum();
        for (int b : B) {
            bSet[b] = true;     // 将b的大小都散列到索引中
        }

        int target = (aTotal + bTotal) / 2;     // 要达到的平衡
        for (int a : A) {
            int b = target - (aTotal - a);      // 如果要替换a，需要的b
            if (b > 0 && b <= 100000 && bSet[b])
                return new int[]{
                        a, b
                };
        }

        return new int[2];
    }

    public static void main(String[] args) {

    }
}
