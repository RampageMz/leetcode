package array.easy_level;

import java.util.HashSet;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/05 16:43
 * <p>
 * Content:    No840_Magic_Squares_In_Grid:
 **/

public class No840_Magic_Squares_In_Grid {

    /**
     * 二维数组里的3*3的子矩阵个数  满足子矩阵的 每行 每列 每条对角线 的和都相等 且3*3矩阵里1-9不重复
     * <p>
     * 3*3 中心的数必须是5 计算所有经过中心数的线  都是15  列式可得中间数为5
     * <p>
     * 遍历数组判断
     *
     * @param grid 二维矩阵
     * @return 满足条件的子矩阵个数
     */
    public int numMagicSquaresInside(int[][] grid) {
        int res = 0;

        if (grid == null || grid.length < 3 || grid[0].length < 3)
            return 0;

        for (int i = 0; i < grid.length - 2; i++) {
            for (int j = 0; j < grid[0].length - 2; j++) {
                res += isSubMatirx(grid, i, j);
            }
        }

        return res;
    }

    public int isSubMatirx(int[][] matrix, int i, int j) {
        int row = matrix.length, col = matrix[0].length;

        int diag = matrix[i][j] + matrix[i + 1][j + 1] + matrix[i + 2][j + 2];    // 主对角线
        int anti_diag = matrix[i + 2][j] + matrix[i + 1][j + 1] + matrix[i][j + 2];    // 反对角线

        if (diag != anti_diag)
            return 0;

        HashSet<Integer> set = new HashSet<>();
        for (int ii = 0; ii < 3; ++ii) {
            for (int jj = 0; jj < 3; ++jj) {
                if (!set.add(matrix[i + ii][j + jj]) || matrix[i + ii][j + jj] > 9 || matrix[i + ii][j + jj] < 1)
                    return 0;
            }
        }

        for (int k = 0; k < 3; ++k) {
            int row_sum = matrix[i][j] + matrix[i][j + 1] + matrix[i][j + 2];
            int col_sum = matrix[i][j] + matrix[i + 1][j] + matrix[i + 2][j];
            if (row_sum != col_sum || row_sum != diag)
                return 0;
        }

        return 1;
    }

    public static void main(String[] args) {

    }
}
