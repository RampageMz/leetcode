package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/25 13:44
 * <p>
 * Content:    No282_move_zero:
 **/

public class No282_move_zero {

    /**
     * 将数组里的0移动到尾部，其他元素相对顺序不变
     * 两个指针，i找0，j找非0,交换，i++
     *
     * @param nums 数组
     */
    public static void moveZore(int[] nums) {
        int length = nums.length;
        int i = 0, j = 0;

        while (true) {
            // 用i指向0元素，j指向后面的第一个非0元素
            while (i < length && nums[i++] != 0) ;

            // 找到的i要-1，下面的j也是同理
            if (i-- == length) break;

            j = i + 1;
            while (j < length && nums[j++] == 0) ;

            swap(nums, i, --j);

            i++;
        }
    }

    /**
     * Better: 也就比我逻辑清楚点 思路差不多
     * @param nums 数组
     */
    public static void BetterMoveZero(int[] nums) {
        int idx = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                idx = i;
                break;
            }
        }

        if (idx == -1) return;

        int iter = idx + 1;
        while (idx < nums.length && iter < nums.length) {
            while (iter < nums.length && nums[iter] == 0) iter++;
            if (iter == nums.length) break;

            swap(nums, idx, iter);
            idx++;
        }
    }

    public static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public static void main(String[] args) {
        System.out.println("[0, 1, 2, 3, 4, 5, 6, 7, 8]");
        int[] nums2 = new int[]{1, 0, 2, 0, 0, 5, 6, 7, 0};
        int[] nums1 = new int[]{0, 1, 0, 3, 12};
        System.out.println(Arrays.toString(nums1));
        moveZore(nums1);
        //moveZore(nums2);
        System.out.println(Arrays.toString(nums1));
        System.out.println("__________");
        //System.out.println(Arrays.toString(nums2));
    }
}
