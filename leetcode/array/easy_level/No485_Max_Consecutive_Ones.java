package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/29 15:15
 * <p>
 * Content:    No485_Max_Consecutive_Ones:
 **/

public class No485_Max_Consecutive_Ones {

    /**
     * 只有0 1的数组，计算连续的1个数
     *
     * @param nums 数组
     * @return 个数
     */
    public static int findMaxConsecutiveOnes(int[] nums) {
        int result = 0;
        int start = 0, end = 0;
        int i = 0;
        while (i < nums.length) {
            start = i;
            while (i < nums.length && nums[i] == 1) {
                i++;
            }
            if (i != nums.length - 1) {
                end = i;
                result = Math.max(result, end - start);
            } else
                break;

            while (i < nums.length && nums[i] == 0) {
                i++;
            }
        }

        return result;
    }

    public static int BetterFindMaxConsecutiveOnes(int[] nums){
        int maxHere = 0, max = 0;
        for (int n : nums)
            max = Math.max(max, maxHere = n == 0 ? 0 : maxHere + 1);
        return max;
    }

    public static void main(String[] args) {
        System.out.println(findMaxConsecutiveOnes(new int[]{1, 0}));
    }
}
