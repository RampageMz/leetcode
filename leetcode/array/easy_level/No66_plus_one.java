package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/23 14/52
 * <p>
 * Content:    No66_plus_one:
 **/

public class No66_plus_one {

    /**
     * 一个整数用数组来保存，高位在前面，计算加1后的数组
     *
     * @param digits 输入的数字数组
     * @return
     */
    public static int[] plusOne(int[] digits) {
        int[] result = digits;
        int length = digits.length;

        if (result[length - 1] != 9) {  // 123+1  124
            result[length - 1] += 1;
        } else {
            // 19+1=20  999+1=1000
            result[length - 1] = 0;

            boolean needOneMore = true;     // 是否需要多加一位
            for (int i = length - 2; i >= 0; i--) {
                result[i] += 1;
                if (result[i] <= 9) {
                    needOneMore = false;
                    break;
                } else {
                    result[i] = 0;
                }
            }

            if (needOneMore) {
                int[] newResult = new int[length + 1];
                newResult[0] = 1;
                for (int i = 1; i < newResult.length; i++) {
                    newResult[i] = result[i - 1];
                }
                return newResult;
            }
        }

        return result;
    }

    /**
     * Better One
     * @param digits
     * @return
     */
    public int[] BetterPlusOne(int[] digits) {
        int n = digits.length;
        for (int i = n - 1; i >= 0; i--) {
            if (digits[i] < 9) {
                digits[i]++;
                return digits;
            }
            digits[i] = 0;
        }
        int[] arr = new int[n + 1];
        arr[0] = 1;
        return arr;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(plusOne(new int[]{1,2,3})));
        System.out.println(Arrays.toString(plusOne(new int[]{4,2,3,1})));
        System.out.println(Arrays.toString(plusOne(new int[]{9,9})));
        System.out.println(Arrays.toString(plusOne(new int[]{1,9})));

    }
}
