package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/27 14:56
 * <p>
 * Content:    No561_Array_Partition_I:
 **/

public class No561_Array_Partition_I {

    /**
     * 输入为2N长度的数组，分成N个对，使得每个对的较小值相加的到的值最大
     *
     * @param nums 数组
     * @return
     */
    public int arrayPairSum(int[] nums) {
        int sum = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++, i++) {
            sum += nums[i];
        }
        return sum;
    }

    /**
     * Better
     * 空间换时间，根据大小平铺在数组的索引上 bool判断是否相加
     *
     * @param nums 数组
     * @return
     */
    public static int BetterArrayPairSum(int[] nums) {
        int[] exist = new int[20001];
        for (int i = 0; i < nums.length; i++) {
            exist[nums[i] + 10000]++;   //题目中设定了每个数范围在-10000~10000，这样将所有数根据值大小，平铺在数组index中
        }
        int sum = 0;
        boolean odd = true;     //根据这个值来判断是否计算总和，按理说应该是按大小一次隔一次的加
        for (int i = 0; i < exist.length; i++) {
            while (exist[i] > 0) {
                if (odd) {
                    sum += i - 10000;
                }
                odd = !odd;
                exist[i]--;     // 有可能多个相同的值
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] a = new int[]{1, 4, 3, 2};
        System.out.println(BetterArrayPairSum(a));
    }
}
