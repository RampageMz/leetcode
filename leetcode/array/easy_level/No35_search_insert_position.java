package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/22 21/20
 * <p>
 * Content:    No35_search_insert_position: https://leetcode.com/problems/search-insert-position/
 **/

public class No35_search_insert_position {

    /**
     * 遍历，找到第一个比target大的数
     * <p>
     * beat 20%
     *
     * https://www.cnblogs.com/luoxn28/p/5767571.html
     *
     * @param nums   排序数组
     * @param target 要找的数
     * @return
     */
    public static int searchInsert(int[] nums, int target) {
        int low = 0, high = nums.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;     // TODO: 2018/11/22 优化：mid=low+(high-low)/2 或者>>>1 防止大位数相加溢出
            if (nums[mid] == target) return mid;
            else if (nums[mid] > target)
                high = mid - 1;
            else
                low = mid + 1;
        }
        return low;
    }

    public static void main(String[] args) {
        //System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 5));
        System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 2));
        //System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 7));
        //System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 0));
    }
}
