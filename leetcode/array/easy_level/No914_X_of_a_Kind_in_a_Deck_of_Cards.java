package array.easy_level;

import java.util.HashMap;
import java.util.Map;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/06 15:53
 * <p>
 * Content:    No914_X_of_a_Kind_in_a_Deck_of_Cards:
 **/

public class No914_X_of_a_Kind_in_a_Deck_of_Cards {

    /**
     * 给定一副牌，每张牌上都写着一个整数。
     此时，你需要选定一个数字 X，使我们可以将整副牌按下述规则分成 1 组或更多组：

     每组都有 X 张牌。
     组内所有的牌上都写着相同的整数。
     仅当你可选的 X >= 2 时返回 true
     *
     * @param deck
     * @return
     */
    public boolean hasGroupsSizeX(int[] deck) {
        Map<Integer, Integer> count = new HashMap<>();
        int res = 0;
        for (int i : deck) count.put(i, count.getOrDefault(i, 0) + 1);
        for (int i : count.values()) res = gcd(i, res);
        return res > 1;
    }

    /**
     * 最大公约数
     *
     * @param a
     * @param b
     * @return
     */
    public int gcd(int a, int b) {
        return b > 0 ? gcd(b, a % b) : a;
    }

    public int gcdBetter(int x, int y) {
        return x == 0 ? y : gcdBetter(y % x, x);
    }

    /**
     * Better
     *
     * @param deck
     * @return
     */
    public boolean BetterHasGroupsSizeX(int[] deck) {
        int[] count = new int[10000];
        for (int c : deck)
            count[c]++;

        int g = -1;
        for (int i = 0; i < 10000; ++i)
            if (count[i] > 0) {
                if (g == -1)
                    g = count[i];
                else
                    g = gcd(g, count[i]);
            }

        return g >= 2;
    }

    public static void main(String[] args) {

    }
}
