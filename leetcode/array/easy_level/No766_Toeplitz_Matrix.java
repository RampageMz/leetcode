package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/28 14:18
 * <p>
 * Content:    No766_Toeplitz_Matrix:
 **/

public class No766_Toeplitz_Matrix {

    /**
     * 判断左上到右下方向的各条线上的数是否相同
     *
     * @param matrix 二位矩阵
     * @return
     */
    public boolean isToeplitzMatrix(int[][] matrix) {
        int line = matrix.length;
        int row = matrix[0].length;

        for (int i = 0; i < line - 1; i++) {
            for (int j = 0; j < row - 1; j++) {
                if (matrix[i][j] != matrix[i + 1][j + 1])
                    return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {

    }
}
