package array.easy_level;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/29 15:40
 * <p>
 * Content:    No448_Find_All_Numbers_Disappeared_in_an_Array:
 **/

public class No448_Find_All_Numbers_Disappeared_in_an_Array {

    /**
     * 给定一个范围在  1 ≤ a[i] ≤ n ( n = 数组大小 ) 的 整型数组
     * 找到所有在 [1, n] 范围之间没有出现在数组中的数字
     *
     * @param nums 数组
     * @return 返回没有出现过的数字
     */
    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> list = new ArrayList<>();

        // 数字在1~n 但是下标从0开始
        for (int i = 0; i < nums.length; i++) {
            int val = Math.abs(nums[i]) - 1;
            if (nums[val] > 0) {
                nums[val] = -nums[val];     // 将整数对应下标的数变为负数，代表着这个整数已经出现过 整数4对应下标3（数组的第四个数字）
            }
        }

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                list.add(i + 1);
            }
        }

        return list;
    }

    public static void main(String[] args) {
        System.out.println(findDisappearedNumbers(new int[]{4, 3, 2, 7, 8, 2, 3, 1}));
    }
}
