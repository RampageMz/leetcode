package array.easy_level;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/05 21:35
 * <p>
 * Content:    No219_Contains_Duplicate_II:
 **/

public class No219_Contains_Duplicate_II {

    /**
     * 是否存在重复的数的下标i j，使得|i-j|<=k
     *
     * @param nums 数组
     * @param k    距离上限
     * @return T/F
     */
    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        if (nums == null || nums.length == 0)
            return false;

        HashMap<Integer, Integer> map = new HashMap<>();    // 记录nums[i],i
        int len = nums.length;

        for (int i = 0; i < len; ++i) {
            if (!map.containsKey(nums[i]))
                map.put(nums[i], i);
            else {
                int pre = map.get(nums[i]);
                if (i - pre > k)
                    map.put(nums[i], i);
                else
                    return true;
            }
        }

        return false;
    }

    /**
     * Better
     *
     * @param nums
     * @param k
     * @return
     */
    public static boolean BetterContainsNearbyDuplicate(int[] nums, int k) {
        if (nums.length > 1000) return false;
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < nums.length; i++) {
            if (i > k)
                set.remove(nums[i - k - 1]);
            if (!set.add(nums[i]))
                return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(BetterContainsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3}, 2));
        //System.out.println(BetterContainsNearbyDuplicate(new int[]{1, 0, 1, 1}, 1));
        //System.out.println(containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3));
    }
}
