package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/02 15:07
 * <p>
 * Content:    No53_Maximum_Subarray:
 **/

public class No53_Maximum_Subarray {

    /**
     * 找到数组里 连续子数组里和最大的
     *
     * @param nums 数组
     * @return 子数组长度
     */
    public static int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0)
            return 0;

        int maxSoFar = nums[0], maxEndingHere = nums[0];
        for (int i = 1; i < nums.length; ++i) {
            maxEndingHere = Math.max(maxEndingHere + nums[i], nums[i]);
            maxSoFar = Math.max(maxSoFar, maxEndingHere);
        }
        return maxSoFar;
    }

    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }
}
