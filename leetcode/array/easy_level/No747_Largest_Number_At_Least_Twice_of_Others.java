package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/01 16:13
 * <p>
 * Content:    No747_Largest_Number_At_Least_Twice_of_Others:
 **/

public class No747_Largest_Number_At_Least_Twice_of_Others {

    /**
     * 找到数组里的最大数 并且这个数需要比剩下的最大数大两倍，否则返回-1
     *
     * @param nums 数组
     * @return 最大数的下标
     */
    public static int dominantIndex(int[] nums) {
        int max = Integer.MIN_VALUE, max_2 = Integer.MIN_VALUE;
        int max_index = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                max = nums[i];
                max_index = i;
            }
        }

        for (int i = 0; i < nums.length; i++) {
            if (i != max_index && nums[i] > max_2) {
                max_2 = nums[i];
            }
        }

        if (max >= 2 * max_2)
            return max_index;
        else
            return -1;

    }

    /**
     * Better
     * 一次遍历，同时比较max和secondMax
     *
     * @param nums 数组
     * @return 下标
     */
    public static int BetterDominantIndex(int[] nums){
        if(nums == null || nums.length == 0){
            return -1;
        }

        if(nums.length == 1){
            return 0;
        }

        int max = Integer.MIN_VALUE + 1;
        int secondMax = Integer.MIN_VALUE;
        int index = 0;

        for(int i = 0; i < nums.length; i++){
            if(nums[i] > max){      // 同时更新max和secondMax
                secondMax = max;
                max = nums[i];
                index = i;
            } else if(nums[i] != max && nums[i] > secondMax){   // 只更新secondMax
                secondMax = nums[i];
            }
        }
        if(secondMax * 2 <= max){
            return index;
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(dominantIndex(new int[]{0, 0, 2, 3}));
    }
}
