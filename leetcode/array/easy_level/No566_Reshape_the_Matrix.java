package array.easy_level;

import array.util.ToolFunction;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/28 14:47
 * <p>
 * Content:    No566_Reshape_the_Matrix:
 **/

public class No566_Reshape_the_Matrix {

    /**
     * reshape matrix
     *
     * @param nums 二维数组
     * @param r    新的行数
     * @param c    新的列数
     * @return 可以reshape则输出新的矩阵，不能的话输出原数组
     */
    public static int[][] matrixReshape(int[][] nums, int r, int c) {
        int row = nums.length;
        int col = nums[0].length;

        if (r * c != row * col)
            return nums;

        int[][] result = new int[r][c];
        int row_index = 0, col_index = 0;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                result[row_index][col_index] = nums[i][j];
                col_index++;
                if (col_index == c) {
                    col_index = 0;
                    row_index++;
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[][] a = new int[][]{
                {1, 2},
                {3, 4},
                {5, 6}
        };

        ToolFunction.printDoubelArray(matrixReshape(a, 2, 3));
    }
}
