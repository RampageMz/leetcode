package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/06 21:48
 * <p>
 * Content:    No581_Shortest_Unsorted_Continuous_Subarray:
 **/

public class No581_Shortest_Unsorted_Continuous_Subarray {

    /**
     * 找到最短的需要排序的子数组 使得对其排序后 整个数组有序  默认升序
     *
     * @param A
     * @return
     */
    public static int findUnsortedSubarray(int[] A) {
        int n = A.length, beg = -1, end = -2, min = A[n-1], max = A[0];
        for (int i=1;i<n;i++) {
            max = Math.max(max, A[i]);
            min = Math.min(min, A[n-1-i]);
            if (A[i] < max) end = i;
            if (A[n-1-i] > min) beg = n-1-i;
        }
        return end - beg + 1;
    }

    public static void main(String[] args) {
        System.out.println(findUnsortedSubarray(new int[]{2, 6, 4, 8, 10, 9, 15}));
        //System.out.println(findUnsortedSubarray(new int[]{1,3,2,2,2}));
        //System.out.println(findUnsortedSubarray(new int[]{1,2,4,5,3}));
    }
}
