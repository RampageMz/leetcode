package array.easy_level;

import java.util.HashMap;
import java.util.stream.IntStream;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/30 15:57
 * <p>
 * Content:    No697_Degree_of_an_Array:
 **/

public class No697_Degree_of_an_Array {

    /**
     * 数组的度的定义是指数组里任一元素出现频数的最大值
     * 任务是找到与 nums 拥有相同大小的度的最短连续子数组，返回其长度
     *
     * @param nums 数组
     * @return 长度
     */
    public int findShortestSubArray(int[] nums) {
        HashMap<Integer, int[]> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                int[] tmp = map.get(nums[i]);   // 0:出现次数 1：起始 2：结束
                tmp[0]++;
                tmp[2] = i;
            } else
                map.put(nums[i], new int[]{1, i, i});
        }

        int degree = Integer.MIN_VALUE;
        int result = nums.length;
        for (int[] value : map.values()) {
            if (value[0] > degree) {    // 当前出现的频数更高
                degree = value[0];
                result = value[2] - value[1] + 1;
            } else if (value[0] == degree) {    //频数相同 需要比较起止的长度
                result = Math.min(value[2] - value[1] + 1, result);
            }
        }

        return result;
    }

    public static void main(String[] args) {

    }
}
