package array.easy_level;

public class No27_remove_element {

    public static int removeElement(int[] nums, int val) {
        int length = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[length++] = nums[i];
            }
        }

        return length;
    }

    public static void main(String[] args) {
        System.out.println(removeElement(new int[]{3, 2, 4, 3, 1}, 3));
    }
}
