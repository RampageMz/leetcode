package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/26 13:04
 * <p>
 * Content:    No128_best_time_sell_stock:
 **/

public class No128_best_time_sell_stock {

    /**
     * 先找到一个低的价格，在找到一个高的价格，从而计算出最大利润
     *
     * @param prices 价格数组
     * @return
     */
    public static int maxProfit(int[] prices) {
        int maxCur = 0, maxSoFar = 0;
        for (int i = 1; i < prices.length; i++) {
            maxCur = Math.max(0, maxCur += prices[i] - prices[i - 1]);    // 考虑了可能有负数的情况
            maxSoFar = Math.max(maxCur, maxSoFar);
        }
        return maxSoFar;
    }

    public static void main(String[] args) {

    }
}
