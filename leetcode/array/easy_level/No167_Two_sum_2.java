package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/24 14/56
 * <p>
 * Content:    No167_Two_sum_2:
 **/

public class No167_Two_sum_2 {

    /**
     * 有序数组，找到两个相加等于Target
     * 无序数组可以遍历放入HashMap,有序头尾指针
     *
     * @param numbers 数组
     * @param target  目标数
     * @return
     */
    public static int[] twoSum(int[] numbers, int target) {
        int[] result = new int[2];

        if (numbers == null || numbers.length < 2)
            return result;

        int left = 0;
        int right = numbers.length - 1;

        while (left < right) {
            int sum = numbers[left] + numbers[right];
            if (sum == target) {
                // 输出的下标从1开始 没有0
                result[0] = left + 1;
                result[1] = right + 1;
                return result;
            } else if (sum < target) {
                left++;
            } else {
                right--;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSum(new int[]{2, 7, 11, 15}, 9)));
    }
}
