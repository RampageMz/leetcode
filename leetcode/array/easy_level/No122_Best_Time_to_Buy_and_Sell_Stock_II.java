package array.easy_level;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/30 15:19
 * <p>
 * Content:    No122_Best_Time_to_Buy_and_Sell_Stock_II:
 **/

public class No122_Best_Time_to_Buy_and_Sell_Stock_II {

    /**
     * 相比下No128 可以完成多次数据
     *
     * @param prices
     * @return
     */
    public static int maxProfit(int[] prices) {
        int max = 0, maxNow = 0, tmp;

        for (int i = 1; i < prices.length; i++) {
            tmp = prices[i] - prices[i - 1];
            if (tmp > 0)
                max += tmp;
        }

        return max;
    }

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7,1,5,3,6,4}));
        System.out.println(maxProfit(new int[]{1, 2, 3, 4, 5}));
        System.out.println(maxProfit(new int[]{7,6,4,3,1}));
    }
}
