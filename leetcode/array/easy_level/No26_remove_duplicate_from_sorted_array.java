package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/26 14:05
 * <p>
 * Content:    No26_remove_duplicate_from_sorted_array:
 **/

public class No26_remove_duplicate_from_sorted_array {

    /**
     * 有序数组返回无重复的数
     *
     * @param nums 有序数组
     * @return
     */
    public static int removeDuplicates(int[] nums) {
        int length = 0; //作为下标

        if (nums == null || nums.length == 0)
            return 0;

        if (nums.length == 1)
            return 1;

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[++length] = nums[i];
            }
        }

        return length + 1;
    }

    public static void main(String[] args) {
        int[] a = new int[]{1, 1, 2};
        System.out.println(removeDuplicates(a));
        System.out.println(Arrays.toString(a));
        int[] b = new int[]{0, 0, 1, 1, 1, 2, 3, 3,4,5};
        System.out.println(removeDuplicates(b));
        System.out.println(Arrays.toString(b));

    }
}
