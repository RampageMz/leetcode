package array.easy_level;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/11/27 10:38
 * <p>
 * Content:    No905_Sort_Array_By_Parity:
 **/

public class No905_Sort_Array_By_Parity {

    /**
     * 将数组的所有偶数放在前面 奇数放在后面
     * 头尾指针
     *
     * @param A 无序数组
     * @return
     */
    public static int[] sortArrayByParity(int[] A) {
        if (A == null || A.length <= 1)
            return A;

        int left = 0, right = A.length - 1;

        while (left < right) {
            if (A[left] % 2 != 0) { // 奇数
                if (A[right] % 2 == 0) {    //偶数
                    swap(A, left, right);
                } else {
                    right--;
                }
            } else {
                left++;
            }
        }

        return A;
    }

    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void main(String[] args) {
        int[] a = new int[]{3, 1, 2, 4};
        System.out.println(Arrays.toString(sortArrayByParity(a)));
    }
}
