package test;

import java.util.Arrays;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/03 14:49
 * <p>
 * Content:    Test:
 **/

public class Test {
    public static void main(String[] args) {
        long long_num = 1234202320;
        System.out.println(String.valueOf(long_num));

        String[] show = new String[5];
        show[0] = " - ";
        show[1] = "|  ";
        show[2] = " - ";
        show[3] = "|  ";
        show[4] = " - ";
        for (int i = 0; i < show.length; i++) {
            System.out.println(show[i]);
        }
        String[] show2 = new String[5];
        show2[0] = " - ";
        show2[1] = "  |";
        show2[2] = " - ";
        show2[3] = "|  ";
        show2[4] = " - ";
        for (int i = 0; i < show2.length; i++) {
            System.out.println(show2[i]);
        }
        String[] com = combineMatrix(show, show2);
        for (int i = 0; i < com.length; i++) {
            System.out.println(com[i]);
        }
    }

    public static String[] combineMatrix(String[] fisrt, String[] second) {
        StringBuffer stringBuffer;
        for (int i = 0; i < fisrt.length; i++) {
            stringBuffer = new StringBuffer(fisrt[i]);
            stringBuffer.append(second[i]);
            fisrt[i] = stringBuffer.toString();
        }

        return fisrt;
    }
}
