package test.thoughtwork;

import java.util.Scanner;

/**
 * Author:     Ma Zhen
 * Date:       2018/12/03 09:50
 * <p>
 * Content:    Solution:ThoughtWorks Q4
 **/


// 主类
public class Solution {

    public static final String SET_SIZE_2 = "set size 2";
    public static final String SET_SIZE_1 = "set size 1";
    public static final String INPUT_END = "exit";
    public static Scanner in = new Scanner(System.in);

    /**
     * 需求说明：
     * <p>
     * 1. 输入罗马数字字符串 显示出相应的数字 并有两种大小
     * 2. 非法输入用E表示
     * 3. 循环输入
     * 4. I:1 V:5 X:10 L:50 C:100 D:500 M:1000
     *
     * TIP: 本程序的成员函数都用的是static修饰，只是为了调用方便
     *
     * 输入样例：
     * CXXIII
     * MMXVIII
     * set size 2
     * CXXIII
     * MMXVIII
     * set size 1
     * MMXVIII
     * FOOBAR
     * exit
     */

    /**
     * 程序入口
     *
     * @param args 启动参数
     */
    public static void main(String[] args) {
        boolean isSizeTwo = false;
        // 处理输入模块
        while (in.hasNextLine()) {
            String str = in.nextLine();
            // 输入END 或者 end 程序结束
            if (str.equalsIgnoreCase(INPUT_END))
                break;

            // 如果写入的是set size 2 再读一行
            if (str.equals(SET_SIZE_2)) {
                isSizeTwo = true;
                str = in.nextLine();
            } else if (str.equals(SET_SIZE_1)) {
                isSizeTwo = false;
                str = in.nextLine();
            }

            // 具体字符串处理
            RomanProcess process = new RomanProcess(str, isSizeTwo);
            process.inputStringProcess();
        }

        ProgramEnd();
    }

    /**
     * 程序结束语
     */
    public static void ProgramEnd() {
        System.out.println("The program is end!");
    }


}

class RomanProcess {
    public static final char ILLEGAL_CHAR = 'E';

    private String romanString;
    private boolean isSizeTwo;

    public RomanProcess(String romanString, boolean isSizeTwo) {
        this.romanString = romanString;
        this.isSizeTwo = isSizeTwo;
    }

    /**
     * 对输入的字符串的处理
     */
    public void inputStringProcess() {
        if (romanString == null)
            return;

        long sum = 0;
        boolean hasIllegal = false;

        for (int i = 0; i < romanString.length(); i++) {
            char ch = romanString.charAt(i);
            switch (RomanEnum.val(ch)) {
                case I:
                    sum += RomanEnum.I.getValue();
                    break;
                case V:
                    sum += RomanEnum.V.getValue();
                    break;
                case X:
                    sum += RomanEnum.X.getValue();
                    break;
                case L:
                    sum += RomanEnum.L.getValue();
                    break;
                case C:
                    sum += RomanEnum.C.getValue();
                    break;
                case D:
                    sum += RomanEnum.D.getValue();
                    break;
                case M:
                    sum += RomanEnum.M.getValue();
                    break;
                default:
                    hasIllegal = true;
                    break;
            }
            //System.out.println("sum:"+sum);

            // 如果有非法字符直接停止
            if (hasIllegal)
                break;
        }

        consoleOutput(sum, hasIllegal);
    }

    /**
     * 将求和以及非法输入的标识传入
     * <p>
     * 如果有非法字符，直接显示E 不显示其他数字
     *
     * @param sum        求和
     * @param hasIllegal 标识
     */
    public void consoleOutput(long sum, boolean hasIllegal) {
        Print print = new Print(isSizeTwo);

        if (hasIllegal) {
            print.printIllegal(ILLEGAL_CHAR);
            return;
        }

        print.computeSumMatrix(sum);
    }

}

/**
 * 专门处理字符控制台输出具体信息的类
 */
class Print {
    private boolean isSizeTwo;

    public Print(boolean isSizeTwo) {
        this.isSizeTwo = isSizeTwo;
    }

    /**
     * 横向合并两个矩阵 成为第一个
     * 两个矩阵的行数相同
     *
     * @param first  第一个
     * @param second 第二个
     * @return
     */
    public void combineMatrix(String[] first, String[] second) {
        StringBuffer stringBuffer;
        for (int i = 0; i < first.length; i++) {
            stringBuffer = new StringBuffer(first[i]);
            stringBuffer.append(second[i]);
            first[i] = stringBuffer.toString();
        }
    }

    /**
     * 将矩阵在控制台输出
     *
     * @param show 矩阵
     */
    public void consolePrint(String[] show) {
        for (int i = 0; i < show.length; i++) {
            System.out.println(show[i]);
        }
    }

    /**
     * 非法输出 控制台
     *
     * @param illegal 非法标识
     */
    public void printIllegal(char illegal) {
        String[] show;
        if (!isSizeTwo) {
            show = new String[5];
            show[0] = " - ";
            show[1] = "|  ";
            show[2] = " - ";
            show[3] = "|  ";
            show[4] = " - ";
        }else {
            show = new String[7];
            show[0] = " -- ";
            show[1] = "|   ";
            show[2] = "|   ";
            show[3] = " -- ";
            show[4] = "|   ";
            show[5] = "|   ";
            show[6] = " -- ";
        }

        consolePrint(show);
    }

    /**
     * 正常计算完后的控制台输出
     *
     * @param sum 求和整数
     */
    public void computeSumMatrix(long sum) {
        String sum_str = String.valueOf(sum);
        String[] first;
        if (!isSizeTwo) {   // 1倍大小
            first = print_size_one(sum_str.charAt(0) - '0');
            for (int i = 1; i < sum_str.length(); i++) {
                combineMatrix(first, print_size_one(sum_str.charAt(i) - '0'));
            }
        } else {    // 2倍大小
            first = print_size_twice(sum_str.charAt(0) - '0');
            for (int i = 1; i < sum_str.length(); i++) {
                combineMatrix(first, print_size_twice(sum_str.charAt(i) - '0'));
            }
        }
        consolePrint(first);
    }

    /**
     * size 1 时的print
     *
     * @param val 整数
     * @return
     */
    public String[] print_size_one(int val) {
        String[] show = new String[5];
        switch (val) {
            case 0:
                print_0(show);
                break;
            case 1:
                print_1(show);
                break;
            case 2:
                print_2(show);
                break;
            case 3:
                print_3(show);
                break;
            case 4:
                print_4(show);
                break;
            case 5:
                print_5(show);
                break;
            case 6:
                print_6(show);
                break;
            case 7:
                print_7(show);
                break;
            case 8:
                print_8(show);
                break;
            case 9:
                print_9(show);
                break;
        }
        return show;
    }

    /**
     * size 2 时的print
     *
     * @param val 整数
     * @return
     */
    public String[] print_size_twice(int val) {
        String[] show = new String[7];
        switch (val) {
            case 0:
                print_0_2(show);
                break;
            case 1:
                print_1_2(show);
                break;
            case 2:
                print_2_2(show);
                break;
            case 3:
                print_3_2(show);
                break;
            case 4:
                print_4_2(show);
                break;
            case 5:
                print_5_2(show);
                break;
            case 6:
                print_6_2(show);
                break;
            case 7:
                print_7_2(show);
                break;
            case 8:
                print_8_2(show);
                break;
            case 9:
                print_9_2(show);
                break;
        }
        return show;
    }

    /**
     * size 1时的尺寸
     */
    public void print_0(String[] show) {
        show[0] = " - ";
        show[1] = "| |";
        show[2] = "   ";
        show[3] = "| |";
        show[4] = " - ";
    }

    public void print_1(String[] show) {
        show[0] = "   ";
        show[1] = "  |";
        show[2] = "   ";
        show[3] = "  |";
        show[4] = "   ";
    }

    public void print_2(String[] show) {
        show[0] = " - ";
        show[1] = "  |";
        show[2] = " - ";
        show[3] = "|  ";
        show[4] = " - ";
    }

    public void print_3(String[] show) {
        show[0] = " - ";
        show[1] = "  |";
        show[2] = " - ";
        show[3] = "  |";
        show[4] = " - ";
    }

    public void print_4(String[] show) {
        show[0] = "   ";
        show[1] = "| |";
        show[2] = " - ";
        show[3] = "  |";
        show[4] = "   ";
    }

    public void print_5(String[] show) {
        show[0] = " - ";
        show[1] = "|  ";
        show[2] = " - ";
        show[3] = "  |";
        show[4] = " - ";
    }

    public void print_6(String[] show) {
        show[0] = " - ";
        show[1] = "|  ";
        show[2] = " - ";
        show[3] = "| |";
        show[4] = " - ";
    }

    public void print_7(String[] show) {
        show[0] = " - ";
        show[1] = "  |";
        show[2] = "   ";
        show[3] = "  |";
        show[4] = "   ";
    }

    public void print_8(String[] show) {
        show[0] = " - ";
        show[1] = "| |";
        show[2] = " - ";
        show[3] = "| |";
        show[4] = " - ";
    }

    public void print_9(String[] show) {
        show[0] = " - ";
        show[1] = "| |";
        show[2] = " - ";
        show[3] = "  |";
        show[4] = " - ";
    }

    /**
     * size 2时的尺寸
     */
    public void print_0_2(String[] show) {
        show[0] = " -- ";
        show[1] = "|  |";
        show[2] = "|  |";
        show[3] = "    ";
        show[4] = "|  |";
        show[5] = "|  |";
        show[6] = " -- ";
    }

    public void print_1_2(String[] show) {
        show[0] = "    ";
        show[1] = "   |";
        show[2] = "   |";
        show[3] = "    ";
        show[4] = "   |";
        show[5] = "   |";
        show[6] = "    ";
    }

    public void print_2_2(String[] show) {
        show[0] = " -- ";
        show[1] = "   |";
        show[2] = "   |";
        show[3] = " -- ";
        show[4] = "|   ";
        show[5] = "|   ";
        show[6] = " -- ";
    }

    public void print_3_2(String[] show) {
        show[0] = " -- ";
        show[1] = "   |";
        show[2] = "   |";
        show[3] = " -- ";
        show[4] = "   |";
        show[5] = "   |";
        show[6] = " -- ";
    }

    public void print_4_2(String[] show) {
        show[0] = "    ";
        show[1] = "|  |";
        show[2] = "|  |";
        show[3] = " -- ";
        show[4] = "   |";
        show[5] = "   |";
        show[6] = "    ";
    }

    public void print_5_2(String[] show) {
        show[0] = " -- ";
        show[1] = "|   ";
        show[2] = "|   ";
        show[3] = " -- ";
        show[4] = "   |";
        show[5] = "   |";
        show[6] = " -- ";
    }

    public void print_6_2(String[] show) {
        show[0] = " -- ";
        show[1] = "|   ";
        show[2] = "|   ";
        show[3] = " -- ";
        show[4] = "|  |";
        show[5] = "|  |";
        show[6] = " -- ";
    }

    public void print_7_2(String[] show) {
        show[0] = " -- ";
        show[1] = "   |";
        show[2] = "   |";
        show[3] = "    ";
        show[4] = "   |";
        show[5] = "   |";
        show[6] = "    ";
    }

    public void print_8_2(String[] show) {
        show[0] = " -- ";
        show[1] = "|  |";
        show[2] = "|  |";
        show[3] = " -- ";
        show[4] = "|  |";
        show[5] = "|  |";
        show[6] = " -- ";
    }

    public void print_9_2(String[] show) {
        show[0] = " -- ";
        show[1] = "|  |";
        show[2] = "|  |";
        show[3] = " -- ";
        show[4] = "   |";
        show[5] = "   |";
        show[6] = " -- ";
    }


}

/**
 * 记录罗马字符与整数值的对应关系
 */
enum RomanEnum {
    I('I', 1),
    V('V', 5),
    X('X', 10),
    L('L', 50),
    C('C', 100),
    D('D', 500),
    M('M', 1000),
    NONE('N', 0);

    private char roman;
    private int value;

    RomanEnum(char ch, int number) {
        this.roman = ch;
        this.value = number;
    }

    public char getRomanChar() {
        return roman;
    }

    public int getValue() {
        return value;
    }

    public static char getRomanByValue(int value) {
        for (RomanEnum roman : RomanEnum.values()) {
            if (roman.value == value)
                return roman.roman;
        }
        return 'n';
    }

    public static RomanEnum val(char ch) {
        for (RomanEnum one : RomanEnum.values()) {
            if (one.getRomanChar() == ch)
                return one;
        }

        return RomanEnum.NONE;
    }
}
